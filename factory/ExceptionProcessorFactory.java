package dev.rodrigo.module.grpcexception.factory;

import dev.rodrigo.module.grpcexception.interfaces.IExceptionProcessor;
import dev.rodrigo.module.grpcexception.processor.BadRequestExceptionProcessor;
import dev.rodrigo.module.grpcexception.processor.DefaultExceptionProcessor;
import io.grpc.Metadata;
import io.grpc.ServerCall;

public class ExceptionProcessorFactory<ReqT, RespT> {
    private final RuntimeException exception;
    private final ServerCall<ReqT, RespT> serverCall;
    private final Metadata headers;

    public ExceptionProcessorFactory(
        RuntimeException exception,
        ServerCall<ReqT, RespT> serverCall,
        Metadata headers
    ) {
        this.exception = exception;
        this.serverCall = serverCall;
        this.headers = headers;
    }
    
    public IExceptionProcessor getProcessador() {
        String nomeException = exception.getClass().getSimpleName();

        switch (nomeException) {
            case "BadRequestException":
                return new BadRequestExceptionProcessor<>(
                    exception,
                    serverCall,
                    headers
                );
        
            default:
                return new DefaultExceptionProcessor<>(
                    exception,
                    serverCall,
                    headers
                );
        }
    }
}
