package dev.rodrigo.module.grpcexception.interceptor;

import dev.rodrigo.module.grpcexception.factory.ExceptionProcessorFactory;
import io.grpc.ForwardingServerCallListener.SimpleForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.quarkus.grpc.GlobalInterceptor;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
@GlobalInterceptor
public class ExceptionServerInterceptor implements ServerInterceptor {

    @Override
    public <ReqT, RespT> Listener<ReqT> interceptCall(
        ServerCall<ReqT, RespT> serverCall,
        Metadata headers,
        ServerCallHandler<ReqT, RespT> serverCallHandler
    ) {
        ServerCall.Listener<ReqT> listener = serverCallHandler
            .startCall(serverCall, headers);
        
        return new ExceptionHandler<>(listener, serverCall, headers);
    }

    private static class ExceptionHandler<ReqT, RespT> extends SimpleForwardingServerCallListener<ReqT> {
        private final ServerCall<ReqT, RespT> serverCall;
        private final Metadata headers;

        ExceptionHandler(
            ServerCall.Listener<ReqT> listener,
            ServerCall<ReqT, RespT> serverCall,
            Metadata headers
        ) {
            super(listener);
            this.serverCall = serverCall;
            this.headers = headers;
        }

        @Override
        public void onHalfClose() {
            try {
                super.onHalfClose();
            } catch (Exception ex) {
                handleException(ex, serverCall, headers);
                throw ex;
            }
        }

        @Override
        public void onReady() {
            try {
                super.onReady();
            } catch (Exception ex) {
                handleException(ex, serverCall, headers);
                throw ex;
            }
        }

        private void handleException(
            Exception exception,
            ServerCall<ReqT, RespT> serverCall,
            Metadata headers
        ) {
            new ExceptionProcessorFactory<>((RuntimeException) exception, serverCall, headers)
                .getProcessador()
                .processar();
        }
    }
}
