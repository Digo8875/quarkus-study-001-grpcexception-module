package dev.rodrigo.module.grpcexception.processor;

import dev.rodrigo.module.grpcexception.interfaces.IExceptionProcessor;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.Status;

public class DefaultExceptionProcessor<ReqT, RespT> implements IExceptionProcessor {
    private final RuntimeException exception;
    private final ServerCall<ReqT, RespT> serverCall;
    private final Metadata headers;

    public DefaultExceptionProcessor(
        RuntimeException exception,
        ServerCall<ReqT, RespT> serverCall,
        Metadata headers
    ) {
        this.exception = exception;
        this.serverCall = serverCall;
        this.headers = headers;
    }

    @Override
    public void processar() {
        serverCall.close(Status.UNKNOWN, headers);
    }
    
}
