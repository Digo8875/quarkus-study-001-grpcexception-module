package dev.rodrigo.module.grpcexception.processor;

import java.util.Map;

import com.google.protobuf.Any;

import dev.rodrigo.grpc.error.ErrorInfoCustom;
import dev.rodrigo.module.grpcexception.interfaces.IExceptionProcessor;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.Status;
import io.grpc.protobuf.StatusProto;

public class BadRequestExceptionProcessor<ReqT, RespT> implements IExceptionProcessor {
    private final RuntimeException exception;
    private final ServerCall<ReqT, RespT> serverCall;
    private final Metadata headers;

    public BadRequestExceptionProcessor(
        RuntimeException exception,
        ServerCall<ReqT, RespT> serverCall,
        Metadata headers
    ) {
        this.exception = exception;
        this.serverCall = serverCall;
        this.headers = headers;
    }

    @Override
    public void processar() {
        Map<String, String> mapMetadata = Map.of();

        Class<?> exceptionClass = exception.getClass();

        jakarta.ws.rs.core.Response.Status httpStatus = jakarta.ws.rs.core.Response.Status.BAD_REQUEST;

        Status grpcStatus = Status.UNKNOWN;

        var errorInfo = ErrorInfoCustom
            .newBuilder()
            .setExceptionSimpleName(exceptionClass.getSimpleName())
            .setHttpStatus(httpStatus.name())
            .setHttpCode(httpStatus.getStatusCode())
            .setGrpcStatus(grpcStatus.toString())
            .setGrpcCode(grpcStatus.getCode().value())
            .setGrpcCodeName(grpcStatus.getCode().name())
            .setError(exception.getMessage())
            .putAllMetadata(mapMetadata)
            .build();

        com.google.rpc.Status rpcStatus =
            com.google.rpc.Status.newBuilder()
                .setCode(grpcStatus.getCode().value())
                .setMessage(exception.getMessage())
                .addDetails(Any.pack(errorInfo))
                .build();

        var statusRuntimeException = StatusProto.toStatusRuntimeException(rpcStatus);

        var newStatus = Status.fromThrowable(statusRuntimeException);

        Metadata newHeaders = statusRuntimeException.getTrailers();

        serverCall.close(newStatus, newHeaders);
    }
}
